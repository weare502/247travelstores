<?php
/**
 * The Template for displaying all single locations.
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$context['locations'] = Timber::get_posts( new WP_Query( array( 'post_type' => 'location', /* "orderby" => 'title', "order" => 'ASC', */ 'posts_per_page' => -1 ) ) );

// Sort locations from West -> East.
usort( $context['locations'], function( $a, $b ){
	$a = intval($a->west_east_order);
	$b = intval($b->west_east_order);
	if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
} );



Timber::render( 'archive-location.twig', $context );
