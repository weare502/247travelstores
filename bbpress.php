<?php
/**
 * The template for displaying all bbPress pages.
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $wp_query;

// $wp_query->in_the_loop = true;

// $context = Timber::get_context();
// $post = new TimberPost();
// $post->thumbnail = $post->get_thumbnail();
// $context['post'] = $post;
// $context['content'] = twentyfourseven_get_echo('the_content');

// Timber::render( 'bbpress.twig', $context );

get_header();

	while ( have_posts() ) : the_post();

		the_content();

	endwhile;

get_footer();