<?php
/**
 * Template Name: Career Department
 * 
 * The Template for displaying the Careers Marketing Page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

Timber::render( 'career-department.twig', $context );
