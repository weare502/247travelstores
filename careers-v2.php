<?php
/**
 * Template Name: Careers v2
 * 
 * The Template for displaying the Careers Marketing Page
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

// $locations = Timber::get_posts('post_type=location');
$context['openings'] =  Timber::get_posts('post_type=job_opening&posts_per_page=1000');

foreach ($context['openings'] as &$opening ){
	$opening->location = Timber::get_post($opening->get_field('job_location'));
}

Timber::render( 'careers-v2.twig', $context );
