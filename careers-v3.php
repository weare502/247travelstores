<?php
/**
 * Template Name: Careers v3
 * 
 * The Template for displaying the Careers Main Page (landing)
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'careers-v3.twig' );

Timber::render( $templates, $context );