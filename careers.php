<?php
/**
 * Template Name: Careers
 * 
 * The Template for displaying the Careers Marketing Page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$context['openings'] = array();

$locations = Timber::get_posts('post_type=location');
foreach ( $locations as $location ){
	$positions = $location->get_field('open_positions');
	// var_dump($location->title);
	// var_dump($positions);
	if ( is_array( $positions ) ){
		foreach ( $positions as $position ){
			$context['openings'][] = array(
				'title' => $position['position'],
				'date' => strtotime( $position['posted_date'] ),
				'location' => $location->title
			);
		}
	}
}

usort($context['openings'], function($a, $b){
	return $a['date'] - $b['date'];
});

// Sort array by newest instead of oldest, then return the first 3 items.
$context['openings'] = array_slice( array_reverse( $context['openings'] ), 0, 3);

Timber::render( 'careers.twig', $context );
