<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}
$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['interactive_header'] = '';

$context['promos'] = Timber::get_posts(array('post_type' => 'promo', 'posts_per_page' => 1, 'orderby' => 'rand' ) );

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );
