<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class TwentyFourSevenSite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );

		add_action( 'init', function(){
			add_editor_style( get_stylesheet_uri() );
		} );

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		// $context['plugin_content'] = TimberHelper::ob_function( 'the_content' );

		return $context;
	}

	function after_setup_theme(){
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'secondary', 'Secondary Navigation' );
		register_nav_menu( 'intranet', 'Intranet Navigation' );

		// Add Fuel Price Table
		if( function_exists('acf_add_options_page') ) :
			acf_add_options_sub_page( array(
				'page_title' 	=> 'Fuel Prices',
				'menu_title'	=> 'Fuel Price Table',
				'parent_slug'	=> 'edit.php?post_type=location',
				'capability'	=> 'edit_posts',
				'updated_message' => 'Location fuel prices updated.',
				'redirect'		=> false
			));
		endif;
	}

	function enqueue_scripts(){
		wp_enqueue_style( 'magnific-popup-style', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20120207' );
		wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120207', true );
		wp_enqueue_script( 'twentyfourseven-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore', 'magnific-popup', 'jquery-ui-accordion' ), '20161000' );
		wp_enqueue_script( 'twentyfourseven-slick', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.js', array( 'jquery' ), '20120207', true );
		wp_enqueue_style( 'twentyfourseven-slick-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.css', '20120207' );
		wp_enqueue_style( 'twentyfourseven-slick-theme-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick-theme.css', '20120207' );
		wp_localize_script( 'magnific-popup', "wpThemeUrl", get_stylesheet_directory_uri() );

	}

	function admin_head_css(){
		?><style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
			.acf-field-5da530ac6102d .acf-repeater.-row > table > tbody > tr > td, .acf-repeater.-block > table > tbody > tr > td { border-top: 2px solid #00c432 !important; }
		</style><?php
	}

	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Callback function to filter the MCE settings
	function tiny_mce_insert_formats( $init_array ) {  
		// Define the style_formats array
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(
				'title'    => 'Button',
				'selector' => 'a',
				'classes'  => 'button',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-hand-pointer-o'
			),
		);
		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );

		return $init_array;
	}
}

new TwentyFourSevenSite();

function twentyfourseven_render_primary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
	) );
}

function twentyfourseven_render_secondary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'secondary',
		'container' => '',
	) );
}

function twentyfourseven_render_intranet_menu() {
	wp_nav_menu( array(
		'theme_location' => 'intranet',
		'container' => '',
	) );
}

if ( ! function_exists('get_taxonomy_archive_link') ){
	function get_taxonomy_archive_link( $taxonomy ) {
		$tax = get_taxonomy( $taxonomy );
		return get_bloginfo( 'url' ) . '/' . $tax->rewrite['slug'];
	}
}

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// Get the fuel prices entered on the "Update Fuel Prices" Options Page (Master Fuel Table)
// Options Page is in the Location's admin menu (sub page)
function update_fuel_prices() {

	$screen = get_current_screen(); // make sure we're on the right options page
	if( strpos($screen->id, 'acf-options-fuel-price-table') == true ) {

		// setup query args for the current, existing locations (total of 11)
		$args = array(
			'post_type' => 'location',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC'
		);
		$loc_query = new WP_Query( $args );

		// get our location post type field keys
		$loc_fuel_available_key = 'field_5809a9ba8e8f5'; // fuel_availability - Checkbox
		$loc_unleaded_key = 'field_580095114ff69';       // unleaded_cash_price - Number
		$loc_unleaded_88_key = 'field_5d314a346e78c'; 	 // unleaded_88_cash_price - Number
		$loc_diesel_key = 'field_5809a9838e8f4'; 		 // diesel_cash_price - Number
		$loc_bulk_key = 'field_5809aa1e8e8f6'; 			 // bulk_def_cash_price - Number
		$loc_hide_price_key = 'field_5d314d5ace408'; 	 // hide_unleaded_cash_price - True/False
		$loc_biodiesel_key = 'field_5ed132971a87d';		 // biodiesel_percentage - Select

		// loop through locations
		if( $loc_query->have_posts() ) :
			while( $loc_query->have_posts() ) : $loc_query->the_post();
				$post_id = get_the_ID(); // location post id

				// data check - make sure we have data to update with
				if( have_rows('fuel_price_repeater', 'options') ) : the_row();

					// get our options page repeater field values (master table values)
					$op_fuel_available_value = get_sub_field( 'fuel_availability_upd', 'options' );
					$op_unleaded_value = get_sub_field( 'unleaded_cash_price_upd', 'options' );
					$op_unleaded_value = get_sub_field( 'unleaded_cash_price_upd', 'options' );
					$op_unleaded_88_value = get_sub_field( 'unleaded_88_cash_price_upd', 'options' );
					$op_diesel_value = get_sub_field( 'diesel_cash_price_upd', 'options' );
					$op_bulk_value = get_sub_field( 'bulk_def_cash_price_upd', 'options' );
					$op_hide_price_value = get_sub_field( 'hide_unleaded_cash_price_upd', 'options' );
					$op_biodiesel_value = get_sub_field( 'biodiesel_percentage_upd', 'options' );

					// update the location field values with the master table values
					update_field( $loc_fuel_available_key, $op_fuel_available_value, $post_id );
					update_field( $loc_unleaded_key, $op_unleaded_value, $post_id );
					update_field( $loc_unleaded_88_key, $op_unleaded_88_value, $post_id );
					update_field( $loc_diesel_key, $op_diesel_value, $post_id );
					update_field( $loc_bulk_key, $op_bulk_value, $post_id );
					update_field( $loc_hide_price_key, $op_hide_price_value, $post_id );

					// only update the biodiesel percentage if the biodiesel option is checked
					if( in_array('biodiesel', $op_fuel_available_value) ) :
						update_field( $loc_biodiesel_key, $op_biodiesel_value, $post_id );
					elseif( ! in_array('biodiesel', $op_fuel_available_value) ):
						$op_biodiesel_value = '';
						update_field( $loc_biodiesel_key, $op_biodiesel_value, $post_id );
					endif;

				endif;
			endwhile;
		endif;
	} // end of screen check
}

// when the update button is clicked - run the master fuel table function
add_action( 'acf/save_post', 'update_fuel_prices' );