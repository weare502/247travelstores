<?php
/**
 * Template Name: Intranet
 * 
 * The Template for displaying the Intranet Pages. This includes the intranet menu.
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 */

if ( ! is_user_logged_in() ){
	wp_die('You must be logged in to view this page. <a href="/employee-login">Click here</a> to login.');
}

$context = Timber::get_context();
$context['user_locations_old'] = Timber::get_posts( get_field( 'store_location', 'user_' . get_current_user_id() ) );

if ( isset( $_POST['update-positions'] ) ){
	$to_update = array();
	foreach ( $context['user_locations_old'] as $location ){
		$to_update[$location->ID] = array();
	}

	foreach ( $_POST['positions'] as $position => $value ){
		$values = explode( "_", sanitize_key($position) );
		$location_id = intval($values[0]);
		$position_id = intval($values[1]);
		
		if ( empty($to_update[$location_id]) ){
			$to_update[$location_id] = array();
		}

		$to_update[$location_id][$position_id] = true;
	}
	// var_dump($to_update);
	foreach ( $to_update as $location => $value ){
		update_post_meta( $location, 'open_positions', $value );
	}
}


$post = Timber::query_post();
$context['post'] = $post;
$context['user_locations'] = Timber::get_posts( get_field( 'store_location', 'user_' . get_current_user_id() ) );
$context['locations'] = Timber::get_posts( new WP_Query( array( 'post_type' => 'location', "orderby" => 'title', "order" => 'ASC', 'posts_per_page' => -1 ) ) );
$context['positions'] = Timber::get_posts( new WP_Query( array( 'post_type' => 'job_position', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ) );
$context['messages'] = Timber::get_posts( new WP_Query( array( 'post_type' => 'bulletin', 'posts_per_page' => -1 ) ) );
$user = wp_get_current_user();

// check for any values in the $user->roles array
if ( count( array_intersect( array('administrator', 'area_manager', 'manager', 'editor'), $user->roles ) ) > 0 ) {
  $context['can_edit_schedules'] = true;
  $context['can_view_dashboard'] = true;
}

if ( current_user_can('edit_post', $post->ID ) ){
	$context['can_edit_post'] = true;
}

Timber::render( 'intranet.twig', $context );
