<?php
/**
 * Template Name: Career Openings
 * 
 * The Template for displaying the Career Openings Page
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

$careers = get_field('job_openings', 'option');
$careers = Timber::get_posts( array('post_type' => 'job_position', 'posts_per_page' => 500, 'no_found_rows' => true ) );

// foreach ( $careers as &$career ){
// 	// Page ID of Application
// 	$app_url = get_permalink( 625 );
//  	$app_url = add_query_arg( 'position', urlencode( $career['title'] ), $app_url );
//  	foreach ( $career['locations'] as $location ){
// 		$app_url = add_query_arg( urlencode( 'locations[]' ), urlencode( get_the_title( $location->ID ) ), $app_url );
// 	}
// 	$app_url = esc_url( $app_url );
// 	$career['application_link'] = $app_url;
// }
// // Clear out the reference from L17.
// unset($career);

$context['careers'] = $careers;

Timber::render( 'openings.twig', $context );
