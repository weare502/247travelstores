<?php

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

$context['location'] = Timber::get_post( $post->get_field('job_location') );

Timber::render( 'single-job-opening.twig', $context );
