<?php
/**
 * The Template for displaying all single locations.
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$context['services'] = $post->terms('location_services');

Timber::render( 'location.twig', $context );
