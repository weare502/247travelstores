/* jshint devel: true */
/* jshint ignore:start */
/**
 * String.startsWith shim
 */
if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position){
			position = position || 0;
			return this.substr(position, searchString.length) === searchString;
	};
}

/**
 * FitVids JS
 */
;(function( $ ){

	'use strict';

	$.fn.fitVids = function( options ) {
		var settings = {
			customSelector: null,
			ignore: null
		};

		if(!document.getElementById('fit-vids-style')) {
			// appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
			var head = document.head || document.getElementsByTagName('head')[0];
			var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
			var div = document.createElement("div");
			div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
			head.appendChild(div.childNodes[1]);
		}

		if ( options ) {
			$.extend( settings, options );
		}

		return this.each(function(){
			var selectors = [
				'iframe[src*="player.vimeo.com"]',
				'iframe[src*="youtube.com"]',
				'iframe[src*="youtube-nocookie.com"]',
				'iframe[src*="kickstarter.com"][src*="video.html"]',
				'object',
				'embed'
			];

			if (settings.customSelector) {
				selectors.push(settings.customSelector);
			}

			var ignoreList = '.fitvidsignore';

			if(settings.ignore) {
				ignoreList = ignoreList + ', ' + settings.ignore;
			}

			var $allVideos = $(this).find(selectors.join(','));
			$allVideos = $allVideos.not('object object'); // SwfObj conflict patch
			$allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

			$allVideos.each(function(){
				var $this = $(this);
				if($this.parents(ignoreList).length > 0) {
					return; // Disable FitVids on this video.
				}
				if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
				if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
				{
					$this.attr('height', 9);
					$this.attr('width', 16);
				}
				var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
						width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
						aspectRatio = height / width;
				if(!$this.attr('name')){
					var videoName = 'fitvid' + $.fn.fitVids._count;
					$this.attr('name', videoName);
					$.fn.fitVids._count++;
				}
				$this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
				$this.removeAttr('height').removeAttr('width');
			});
		});
	};
	
	// Internal counter for unique video names.
	$.fn.fitVids._count = 0;
	
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );
/* jshint ignore:end */

jQuery( document ).ready( function( $ ) {

	var $menuToggle = $( '#menu-toggle' );
	var $body = $('body');
	var $header = $('#header');

	$body.fitVids();

	$body.on('click', '#menu-toggle', function(){
		$header.toggleClass('nav-open');
		$menuToggle.toggleClass('fa-bars');
		$menuToggle.toggleClass('fa-close');
		$header.find('.clicked-once').removeClass('clicked-once');
		$header.find('.sub-menu').css('display', 'none');
	} );

	$('.home .reviews').slick({
		arrows: true,
		// adaptiveHeight: true,
		// slide: '.review',
		autoplay: true,
		autoplaySpeed: 6000
	});

	$('.menu-item-has-children > a').click(function(e){
		var width = $(window).width();

		// Don't expand on desktop
		if ( width > 1300 ){
			return;
		}

		if ( $(e.target).hasClass('clicked-once') ){
			return;
		} else {
			e.preventDefault();
			$(e.target).siblings('.sub-menu').slideDown();
			$(e.target).addClass('clicked-once');
		}
	});

	$('#content').find('a').has('img').addClass('imgwrap');
							

	$('.open-popup-link').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});
	
	$('.magnific-trigger').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});

	$('#menu-item-72 a').on('click', function(e){
		e.preventDefault();
		$('.call-to-action-cards .magnific-trigger').click();
	});

	function showTab(e){
		e.preventDefault();
		$('.tab-content').hide();
		$('.tab-switch').removeClass('active-tab');
		var $this = $(this);
		$this.addClass('active-tab');
		var target = $this.attr('data-tab-target');
		$('[data-tab=' + target + ']').show();
	}

	$('.tab-switch').on('click', showTab);

	$('.tab-content').hide();
	$('.tab-content').first().show();
	$('.tabs .tab-switch').removeClass('active-tab');
	$('.tabs .tab-switch').first().addClass('active-tab');

	function makeUSD( num ){
		return "$" + num + "9";
	}

	// Single Location Scripts.
	if ( $('body').hasClass('single-location') ){

		var $fuelPricesContainer = $('.fuel-prices');
		var unleadedCashPrice = parseFloat( $('.fuel-prices').attr('data-unleaded-cash-price') );
		var unleaded88CashPrice = parseFloat( $('.fuel-prices').attr('data-unleaded-88-cash-price') );
		var dieselCashPrice = parseFloat( $('.fuel-prices').attr('data-diesel-cash-price') );
		var $unleadedPrice = $('#unleaded-price'),
			$unleaded88Price = $('#unleaded-88-price'),
			$ethanolFreePrice = $('#ethanol-free-price'),
			$midGradePrice = $('#mid-grade-price'),
			$premiumPrice = $('#premium-price'),
			$dieselPrice = $('#diesel-price'),
			$bulkDefPrice = $('#bulk-def-price');

		$bulkDefPrice = $bulkDefPrice;

		$unleadedPrice.text( makeUSD( unleadedCashPrice.toFixed(2) ) );
		$midGradePrice.text( makeUSD( (unleadedCashPrice + 0.25).toFixed(2) ) );
		$ethanolFreePrice.text( makeUSD( (unleadedCashPrice + 0.35).toFixed(2) ) );
		$premiumPrice.text( makeUSD( (unleadedCashPrice + 0.50).toFixed(2) ) );
		// Make it's own
		debugger
		$dieselPrice.text( makeUSD( (dieselCashPrice).toFixed(2) ) );
		$unleaded88Price.text( makeUSD( unleaded88CashPrice.toFixed(2) ) );

		$fuelPricesContainer.on( 'click', '.tab', function(){
			var newTab = $(this).attr( 'data-tab' );
			$fuelPricesContainer.attr( 'data-tab', $(this).attr( 'data-tab' ) );
			history.replaceState({}, location.title, "#" + $(this).attr( 'data-tab' ) );
			
			switch(newTab) {
				case 'cash':
					console.log('cash works');
					$unleadedPrice.text( makeUSD( (unleadedCashPrice + 0.00).toFixed(2) ) );
					$midGradePrice.text( makeUSD( (unleadedCashPrice + 0.25).toFixed(2) ) );
					$ethanolFreePrice.text( makeUSD( (unleadedCashPrice + 0.35).toFixed(2) ) );
					$premiumPrice.text( makeUSD( (unleadedCashPrice + 0.50).toFixed(2) ) );
					//
					$dieselPrice.text( makeUSD( (dieselCashPrice).toFixed(2) ) );
					$unleaded88Price.text( makeUSD( (unleaded88CashPrice + 0.00).toFixed(2) ) );
					break;
				case 'debit':
					console.log('debit works');
					$unleadedPrice.text( makeUSD( (unleadedCashPrice + 0.05).toFixed(2) ) );
					$midGradePrice.text( makeUSD( (unleadedCashPrice + 0.30).toFixed(2) ) );
					$ethanolFreePrice.text( makeUSD( (unleadedCashPrice + 0.40).toFixed(2) ) );
					$premiumPrice.text( makeUSD( (unleadedCashPrice + 0.55).toFixed(2) ) );
					// Debit is same as cash for Diesel
					$dieselPrice.text( makeUSD( (dieselCashPrice + 0).toFixed(2) ) );
					$unleaded88Price.text( makeUSD( (unleaded88CashPrice + 0.05).toFixed(2) ) );
					break;
				case 'credit':
					console.log('credit works');
					$unleadedPrice.text( makeUSD( (unleadedCashPrice + 0.10).toFixed(2) ) );
					$midGradePrice.text( makeUSD( (unleadedCashPrice + 0.35).toFixed(2) ) );
					$ethanolFreePrice.text( makeUSD( (unleadedCashPrice + 0.45).toFixed(2) ) );
					$premiumPrice.text( makeUSD( (unleadedCashPrice + 0.60).toFixed(2) ) );
					//
					$dieselPrice.text( makeUSD( (dieselCashPrice + 0.07).toFixed(2) ) );
					$unleaded88Price.text( makeUSD( (unleaded88CashPrice + 0.10).toFixed(2) ) );
					break;
			}
		} );

		if ( location.pathname.startsWith('/location') && ( location.hash.includes('cash') || location.hash.includes('credit') || location.hash.includes('debit') ) ){
			$('.fuel-prices').attr('data-tab', location.hash.slice(1) );
			$('.fuel-prices .tab.' + location.hash.slice(1) ).click();
		}

		// Get weather for zip code
		var locationZipCode = $( '#weather-info' ).attr( 'data-zip-code' );
		$.ajax( {
			url : "//api.wunderground.com/api/7f63c6ff6ed971d2/conditions/q/" + locationZipCode + ".json",
			dataType : "jsonp",
			success : function(parsed_json) {
				var wpThemeUrl = window.wpThemeUrl || "";
				var temp_f = parsed_json.current_observation.temp_f;
				var night = parsed_json.current_observation.icon_url.includes( 'nt_' ) ? 'nt_' : '';
				var description = parsed_json.current_observation.weather;

				$('#weather-icon').attr( 'src', wpThemeUrl + "/static/images/weather-icons/" + night + parsed_json.current_observation.icon + '.png' );
				$('#weather-temp').html( temp_f + "&deg;" );
				$('#weather-description').text( description );
			}
		} );
	
	} // End Single Location Scripts.


	// Location Archive Scripts
	if ( $('body').hasClass('post-type-archive-location') ){
		var $table = $('.locations-table');
		var $locations = $($table).find('.location');

		$locations.each(function( index, el ){
			var $el = $(el);
			var $unleadedPrice = parseFloat( $el.find('.unleaded-price').html().trim() );
			// var $regularUnleaded = $el.find('.regular-unleaded-price');
			// var $midGrade = $el.find('.mid-grade-price');
			// var $premium = $el.find('.premium-price');
			var $diesel = $el.find('.diesel-price');
			var $bulkDef = $el.find('.bulk-def-price');

			// Calculate & Format Ethanol Free Price.
			// if ( $regularUnleaded.html().trim().length === 0 ){
			// 	$regularUnleaded.html( "$" + ( $unleadedPrice + 0.40 ).toFixed(2) + "9" );
			// }

			// Calculate & Format Mid Grade Price.
			// if ( $midGrade.html().trim().length === 0 ){
			// 	$midGrade.html( "$" + ( $unleadedPrice + 0.30 ).toFixed(2) + "9" );
			// }

			// Calculate & Format Premium Price.
			// if ( $premium.html().trim().length === 0 ){
			// 	$premium.html( "$" + ( $unleadedPrice + 0.55 ).toFixed(2) + "9" );
			// }

			// Format Unleaded Price. Add 5¢ because we want to show the "Debit" Price.
			$el.find('.unleaded-price').html( "$" + ( $unleadedPrice + 0.05 ).toFixed(2) + "9" );

			// Format Diesel Price.
			$diesel.html( $diesel.html().trim() );
			$diesel.prepend("$");

			// Add $ to Bulk DEF Price if necessary.
			$bulkDef.html($bulkDef.html().trim());
			if ( $bulkDef.html() !== "--" ){
				$bulkDef.prepend("$");
			}
		}); // End format location prices.

	}

	$('.reward-card-categories a').each(function(index, el){
		if ( el.href === window.location.href ) {
			$(el).addClass('current');
			return false;
		}
	});

	$('#calculate-savings').on('click', function(){
		var $loader = $('.rewards-calculator .loader');
		var $savings = $('.rewards-calculator .savings');
		var gas = parseInt( $('.rewards-calculator #gas').val() ) || 0;
		var diesel = parseInt( $('.rewards-calculator #diesel').val() ) || 0;
		var gasDiscount = 0.10;
		var dieselDiscount = 0.12;
		var total = 0;

		$savings.slideUp();
		$loader.slideDown();

		if ( 2500 <= gas ) {
			// console.log(gas + " is equal to or more than 2500");
			gasDiscount = 0.12;
		} else if ( 1500 <= gas ) {
			// console.log(gas + " is equal to or more than 1500");
			gasDiscount = 0.11;
		} 

		if ( 2500 <= diesel ) {
			// console.log(diesel + " is equal to or more than 2500 - diesel");
			dieselDiscount = 0.14;
		} else if ( 1500 <= diesel ) {
			// console.log(diesel + " is equal to or more than 1500 - diesel");
			dieselDiscount = 0.13;
		}

		total = ( (gas * gasDiscount * 52) + (diesel * dieselDiscount * 52) ).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

		// console.log( total );

		$savings.find('span').text(total);

		window.setTimeout(function(){
			$loader.slideUp();
			$savings.slideDown();
		}, 1000 );
	});

	$( ".careers-accordion" ).accordion({
	  collapsible: true,
	  active: false
	});

	$('a').each(function() {
	   var a = new RegExp('/' + window.location.host + '/');
	   if(!a.test(this.href)) {
	       $(this).click(function(event) {
	           event.preventDefault();
	           event.stopPropagation();
	           window.open(this.href, '_blank');
	       });
	   }
	});

}); // End Document Ready

/*
// Home Page Loading Screen
(function($){
	var $body = jQuery('body');

	// Skip the loading animation for logged-in users, ie. admins.
	if ( $body.hasClass('admin-bar') ){
		return;
	}
	
	$body.prepend("<div class='loading-animation'></div>");

	var $loader = $('.loading-animation');

	$body.addClass('loading-animation-playing');
	
	$(window).on('load', function(){
		window.setTimeout(function(){
			$loader.css('opacity', 0);
			$body.removeClass('loading-animation-playing');
			window.setTimeout(function(){
				$loader.remove();
			}, 1100);
		}, 2000 );
	});
})(jQuery);

// Location map with geocode
var mapCity = $( '#location-map' ).attr( 'data-location' );
$.ajax( {
	url : "https://maps.googleapis.com/maps/api/geocode/json?address=" + mapCity + "&key=AIzaSyDA4C6wHhbFIEm0hcGwz5IhGqUC5P7Ez3E",
	dataType : "json",
	success : function(data) {
		console.log(data);
		if ( data.status !== "OK" ){
			return;
		}

		var location = data.results[0].geometry.location;

		var map;
		var google = window.google || {};

		map = new google.maps.Map(document.getElementById('location-map'), {
			center: { lat: location.lat, lng: location.lng },
			zoom: 13
		});

	}

} );



 */