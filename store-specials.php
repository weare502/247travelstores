<?php
/**
 * Template Name: Store Specials
 * 
 * The Template for displaying the sitewide Store Specials.
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

Timber::render( 'store-specials.twig', $context );
