<?php
/**
 * The Template for displaying all single locations.
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
// $context['term'] = Timber::get_term('reward_card_category');
$context['title'] = "Rewards &amp; Savings";
$context['terms'] = Timber::get_terms('reward_card_category');
$context['cards'] = Timber::get_posts();

Timber::render('reward-card-archive.twig', $context);
