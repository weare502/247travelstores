<?php
/**
 * Template Name: Traveler Info
 * 
 * The Template for displaying the Career Openings Page
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
// Get Road Tips Posts
$context['posts'] = Timber::get_posts( array( 'cat' => '13' ) );

Timber::render( 'traveler-info.twig', $context );
